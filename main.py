from flask import Flask
from flask import jsonify 
from flask_restful import Resource, Api
from cryptography.fernet import Fernet
import boto3
import string
import random
import paramiko
import logging
import time
import mysql.connector




#mydb = mysql.connector.connect(
  #host="localhost",
  #user="root",
  #password="123456",
  #database="adminipos"
#)

app = Flask(__name__)
api = Api(app)

class Quotes(Resource):
    def get(self):
        return {
            'William Shakespeare': {
                'quote': ['Love all,trust a few,do wrong to none',
                'Some are born great, some achieve greatness, and some greatness thrust upon them.']
        },
        'Linus': {
            'quote': ['Talk is cheap. Show me the code.']
            }
        }


class connectSsh(Resource):
    def get(self,dominioInstancia):
        output_file = 'paramiki.log'
        k = paramiko.RSAKey.from_private_key_file("/Users/martintellez/Documents/Projects/ipos/iposbyivan.pem")
        instance_ip = dominioInstancia
        c = paramiko.SSHClient()
        dominioInstancia = str(dominioInstancia)

        c.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        print ("connecting...")
        c.connect( hostname = instance_ip, username = "ubuntu", pkey = k,)
        print ("connected!!!")
    
        comando = 'sudo mv /var/www/ishop-test1.ivanapps.com /var/www/'+ dominioInstancia
        (stdin, stdout, stderr) = c.exec_command(comando)
        cmd_output = stdout.read()
        print("log printing connection ssh and create folder project::::\n", comando, cmd_output)
        with open(output_file,'w+') as file:
            file.write(str(cmd_output))
            return output_file
       
        return True;

        #c.close()
       



       
class  descargarArchivo(Resource):
    def get(self,dominioInstancia):
            dominioInstancia = str(dominioInstancia)
            instance_ip = dominioInstancia
            ssh = paramiko.SSHClient()
            k = paramiko.RSAKey.from_private_key_file("/Users/martintellez/Documents/Projects/ipos/iposbyivan.pem")

            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.connect(hostname = instance_ip, username = "ubuntu", pkey = k)
            sftp_client = ssh.open_sftp()
            instrcc = '/var/www/'+dominioInstancia+'/application/config'
            sftp_client.chdir(instrcc)
            sftp_client.get('config.php', '/Users/martintellez/Desktop/config_dw.php')
            sftp_client.close()
            ssh.close()
            return True


class descargarIndex(Resource):
    def get(self,dominioInstancia):
        instance_ip = dominioInstancia
        dominioInstancia = str(dominioInstancia)
        ssh = paramiko.SSHClient()
        k = paramiko.RSAKey.from_private_key_file("/Users/martintellez/Documents/Projects/ipos/iposbyivan.pem")

        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(hostname = instance_ip, username = "ubuntu", pkey = k)
        sftp_client = ssh.open_sftp()
        instrucc = '/var/www/'+dominioInstancia+'/application/views/admin/login'
        sftp_client.chdir(instrucc)
        sftp_client.get('license.php', '/Users/martintellez/Desktop/license_dw.php')
        sftp_client.close()
        ssh.close()
        return True
#/etc/apache2/sites-available
class descargarConf(Resource):
    def get(self,dominioInstancia):
        instance_ip = dominioInstancia
        dominioInstancia = str(dominioInstancia)
        ssh = paramiko.SSHClient()
        k = paramiko.RSAKey.from_private_key_file("/Users/martintellez/Documents/Projects/ipos/iposbyivan.pem")

        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(hostname = instance_ip, username = "ubuntu", pkey = k)
        sftp_client = ssh.open_sftp()
        instrucc = '/etc/apache2/sites-available'
        sftp_client.chdir(instrucc)
        sftp_client.get('ishop-test1.ivanapps.com.conf', '/Users/martintellez/Desktop/'+dominioInstancia+'.conf_dw.php')
        sftp_client.close()
        ssh.close()
        return True

class escribirArchivo(Resource):
    def get(self,dominioInstancia):
        dominioInstancia = str(dominioInstancia)
        filedata = None
        with open('/Users/martintellez/Desktop/config_dw.php', 'r') as file :
            filedata = file.read()
            
            # Replace the target string
            fileUrl = 'https://'+dominioInstancia+'/'
            filedata = filedata.replace('https://ishop-test1.ivanapps.com/',fileUrl )
# Write the file out again
        with open('/Users/martintellez/Desktop/config_dw.php', 'w') as file:
            file.write(filedata)
        return True

class escribirIndex(Resource):
    def get(self,dominioInstancia):
        dominioInstancia = str(dominioInstancia)
        filedata = None
        with open('/Users/martintellez/Desktop/license_dw.php', 'r') as file :
            filedata = file.read()
            # Replace the target string
            fileUrl = '@@4@@https://'+dominioInstancia+'/'
            filedata = filedata.replace('@@4@@http://localhost:8080/iposbyivan/', fileUrl)
# Write the file out again
        with open('/Users/martintellez/Desktop/license_dw.php', 'w') as file:
            file.write(filedata)
        return True


class escribirConf(Resource):
    def get(self,dominioInstancia):
        dominioInstancia = str(dominioInstancia)
        filedata = None
        fileEdit = '/Users/martintellez/Desktop/'+dominioInstancia+'.conf_dw.php'
        with open(fileEdit, 'r') as file:
            filedata = file.read()
            # Replace the target string
            fileUrl = dominioInstancia
            filedata = filedata.replace('ishop-test1.ivanapps.com', fileUrl)
            filedata = filedata.replace('RewriteEngine on', '')
# Write the file out againq
        openFile = '/Users/martintellez/Desktop/'+dominioInstancia+'.conf_dw.php'
        with open(openFile, 'w') as file:
            file.write(filedata)
        return True
#sudo rm config.php
#sudo mv config_dw.php config.php

# sudo rm license.php
# sudo mv license_dw.php license.php


class actualizarArchivo(Resource):
    def get(self,dominioInstancia):
        dominioInstancia = str(dominioInstancia)
        instance_ip = dominioInstancia
        ssh = paramiko.SSHClient()
        k = paramiko.RSAKey.from_private_key_file("/Users/martintellez/Documents/Projects/ipos/iposbyivan.pem")
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(hostname = instance_ip, username = "ubuntu", pkey = k)
        sftp_client = ssh.open_sftp()
        updateFile = '/var/www/'+dominioInstancia+'/application/config/config_dw.php'
        sftp_client.put('/Users/martintellez/Desktop/config_dw.php', updateFile)
        sftp_client.close()
        ssh.close()
        return True

class actualizarIndex(Resource):
    def get(self,dominioInstancia):
        dominioInstancia = str(dominioInstancia)
        instance_ip = dominioInstancia
        ssh = paramiko.SSHClient()
        k = paramiko.RSAKey.from_private_key_file("/Users/martintellez/Documents/Projects/ipos/iposbyivan.pem")

        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(hostname = instance_ip, username = "ubuntu", pkey = k)
        sftp_client = ssh.open_sftp()
        updateFile = '/var/www/'+dominioInstancia+'/application/views/admin/login/license_dw.php'
        sftp_client.put('/Users/martintellez/Desktop/license_dw.php', updateFile)
        sftp_client.close()
        ssh.close()
        return True

class actualizarConf(Resource):
    def get(self,dominioInstancia):
        instance_ip = dominioInstancia
        ssh = paramiko.SSHClient()
        k = paramiko.RSAKey.from_private_key_file("/Users/martintellez/Documents/Projects/ipos/iposbyivan.pem")

        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(hostname = instance_ip, username = "ubuntu", pkey = k)
        sftp_client = ssh.open_sftp()
        fileupdateLocal = '/Users/martintellez/Desktop/'+dominioInstancia+'.conf_dw.php'
        fileupdateRemote = '/var/www/tempConf/'+dominioInstancia+'.conf_dw.php'
        sftp_client.put(fileupdateLocal, fileupdateRemote)
        sftp_client.close()
        ssh.close()
        return True

class crearCarpeta(Resource):
    def get(self,dominioInstancia):
        output_file = "paramiki.log"
        dominioInstancia = str(dominioInstancia)
        k = paramiko.RSAKey.from_private_key_file("/Users/martintellez/Documents/Projects/ipos/iposbyivan.pem")
        instance_ip = dominioInstancia
        c = paramiko.SSHClient()
       
        c.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        print ("connecting...")
        c.connect( hostname = instance_ip, username = "ubuntu", pkey = k,)
        print ("connected!!!")

        comandoQ =  "sudo mkdir /var/www/tempConf"
        (stdin, stdout, stderr) = c.exec_command(comandoQ)
        cmd_output = stdout.read()
        print("log printing::::\n", comandoQ, cmd_output)

        comandoD =  "sudo chmod -R 777 /var/www/tempConf"
        (stdin, stdout, stderr) = c.exec_command(comandoD)
        cmd_output = stdout.read()
        print("log printing::::\n", comandoD, cmd_output)


        with open(output_file,'w+') as file:
            file.write(str(cmd_output))
            return output_file
        c.close()

        return True


class comandActualizar(Resource):
    def get(self, dominioInstancia):

        output_file = "paramiki.log"
        dominioInstancia = str(dominioInstancia)
        k = paramiko.RSAKey.from_private_key_file("/Users/martintellez/Documents/Projects/ipos/iposbyivan.pem")
        instance_ip = dominioInstancia
        c = paramiko.SSHClient()
       
        c.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        print ("connecting...")
        c.connect( hostname = instance_ip, username = "ubuntu", pkey = k,)
        print ("connected!!!")

        comandoQ =  "sudo rm /var/www/"+dominioInstancia+"/application/config/config.php"
        (stdin, stdout, stderr) = c.exec_command(comandoQ)
        cmd_output = stdout.read()
        print("log printing::::\n", comandoQ, cmd_output)

        comandoCin =  "sudo mv /var/www/"+dominioInstancia+"/application/config/config_dw.php /var/www/"+dominioInstancia+"/application/config/config.php"
        (stdin, stdout, stderr) = c.exec_command(comandoCin)
        cmd_output = stdout.read()
        print("log printing::::\n", comandoCin, cmd_output)

        with open(output_file,'w+') as file:
            file.write(str(cmd_output))
            return output_file
        c.close()

        return True


class comandActualizarIndex(Resource):
    def get(self, dominioInstancia):

        output_file = "paramiki.log"
        dominioInstancia = str(dominioInstancia)
        k = paramiko.RSAKey.from_private_key_file("/Users/martintellez/Documents/Projects/ipos/iposbyivan.pem")
        instance_ip = dominioInstancia
        c = paramiko.SSHClient()
       
        c.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        print ("connecting...")
        c.connect( hostname = instance_ip, username = "ubuntu", pkey = k,)
        print ("connected!!!")

        comandoQ =  "sudo rm /var/www/"+dominioInstancia+"/application/views/admin/login/license.php"
        (stdin, stdout, stderr) = c.exec_command(comandoQ)
        cmd_output = stdout.read()
        print("log printing::::\n", comandoQ, cmd_output)

        comandoCin =  "sudo mv /var/www/"+dominioInstancia+"/application/views/admin/login/license_dw.php /var/www/"+dominioInstancia+"/application/views/admin/login/license.php"
        (stdin, stdout, stderr) = c.exec_command(comandoCin)
        cmd_output = stdout.read()
        print("log printing::::\n", comandoCin, cmd_output)

        with open(output_file,'w+') as file:
            file.write(str(cmd_output))
            return output_file
        c.close()

        return True

class ultimosCommands(Resource):
    def get(self,dominioInstancia):
        output_file = "paramiki.log"
        dominioInstancia = str(dominioInstancia)
        k = paramiko.RSAKey.from_private_key_file("/Users/martintellez/Documents/Projects/ipos/iposbyivan.pem")
        instance_ip = dominioInstancia
        c = paramiko.SSHClient()
       
        c.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        print ("connecting...")
        c.connect( hostname = instance_ip, username = "ubuntu", pkey = k,)
        print ("connected!!!")
        #bash_script = open("script.sh").read()

        
    

       

        comandoT =  "sudo mv /var/www/tempConf/"+dominioInstancia+".conf_dw.php /etc/apache2/sites-available/"
        (stdin, stdout, stderr) = c.exec_command(comandoT)
        cmd_output = stdout.read()
        print("log printing::::\n", comandoT, cmd_output)

        comandoQ =  "sudo rm /etc/apache2/sites-available/ishop-test1.ivanapps.com.conf"
        (stdin, stdout, stderr) = c.exec_command(comandoQ)
        cmd_output = stdout.read()
        print("log printing::::\n", comandoQ, cmd_output)

        comandoCin =  "sudo mv /etc/apache2/sites-available/"+dominioInstancia+".conf_dw.php /etc/apache2/sites-available/"+dominioInstancia+".conf"
        (stdin, stdout, stderr) = c.exec_command(comandoCin)
        cmd_output = stdout.read()
        print("log printing::::\n", comandoCin, cmd_output)

        comandoSes =  "cd /etc/apache2/sites-available/"
        (stdin, stdout, stderr) = c.exec_command(comandoSes)
        cmd_output = stdout.read()
        print("log printing::::\n", comandoSes, cmd_output)

        comandoSesww =  "sudo a2dissite ishop-test1.ivanapps.com-le-ssl"
        (stdin, stdout, stderr) = c.exec_command(comandoSesww)
        cmd_output = stdout.read()
        print("log printing::::\n", comandoSesww, cmd_output)

        comandoSesw =  "sudo a2dissite ishop-test1.ivanapps.com"
        (stdin, stdout, stderr) = c.exec_command(comandoSesw)
        cmd_output = stdout.read()
        print("log printing::::\n", comandoSesw, cmd_output)


        comandoSe =  "sudo a2ensite "+dominioInstancia+".conf"
        (stdin, stdout, stderr) = c.exec_command(comandoSe)
        cmd_output = stdout.read()
        print("log printing::::\n", comandoSe, cmd_output)

        comandoSie =  "sudo service apache2 start"
        (stdin, stdout, stderr) = c.exec_command(comandoSie)
        cmd_output = stdout.read()
        print("log printing::::\n", comandoSie, cmd_output)


        comandoSies =  "sudo rm /etc/apache2/sites-enabled/nagios.conf"
        (stdin, stdout, stderr) = c.exec_command(comandoSies)
        cmd_output = stdout.read()
        print("log printing::::\n", comandoSies, cmd_output)


        comandoSiess =  "sudo systemctl restart apache2"
        (stdin, stdout, stderr) = c.exec_command(comandoSiess)
        cmd_output = stdout.read()
        print("log printing::::\n", comandoSiess, cmd_output)

        with open(output_file,'w+') as file:
            file.write(str(cmd_output))
            return output_file
        c.close()
       

        return True;


class cerrandoInstanciaCreada(Resource):
    def get(self, dominioInstancia):
        output_file = "paramiki.log"
        dominioInstancia = str(dominioInstancia)
        k = paramiko.RSAKey.from_private_key_file("/Users/martintellez/Documents/Projects/ipos/iposbyivan.pem")
        instance_ip = dominioInstancia
        c = paramiko.SSHClient()
       
        c.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        print ("connecting...")
        c.connect( hostname = instance_ip, username = "ubuntu", pkey = k,)
        print ("connected!!!")
        comandoSie =  "sudo certbot --apache -d "+dominioInstancia
        (stdin, stdout, stderr) = c.exec_command(comandoSie, get_pty = True)
        
        


        print ("Esperando...")
        time.sleep(3)
        print ("1")
        print ("2")
        print ("3")
        stdin.write('2')
        stdin.write('\n')
        stdin.flush()
        cmd_output = stdout.read()
        print("log printing::::\n", comandoSie, cmd_output)
        


        comandoNueve =  "sudo rm -R /var/www/tempConf"
        (stdin, stdout, stderr) = c.exec_command(comandoNueve)
        cmd_output = stdout.read()
        print("log printing::::\n", comandoNueve, cmd_output)
 
        
        c.close()

        return True;

class resetCerbot(Resource):
    def get(self, dominioInstancia):
        output_file = "paramiki.log"
        dominioInstancia = str(dominioInstancia)
        k = paramiko.RSAKey.from_private_key_file("/Users/martintellez/Documents/Projects/ipos/iposbyivan.pem")
        instance_ip = dominioInstancia
        c = paramiko.SSHClient()
       
        c.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        print ("connecting...")
        c.connect( hostname = instance_ip, username = "ubuntu", pkey = k,)
        print ("connected!!!")

        comandoNueve1 = 'sudo find / -type f -name ".certbot.lock"'
        (stdin, stdout, stderr) = c.exec_command(comandoNueve1)
        cmd_output = stdout.read()
        print("log printing::::\n", comandoNueve1, cmd_output)
       

        comandoNueve2 =  'sudo find / -type f -name ".certbot.lock" -exec rm {} \;'
        (stdin, stdout, stderr) = c.exec_command(comandoNueve2)
        cmd_output = stdout.read()
        print("log printing::::\n", comandoNueve2, cmd_output)
       
        comandoSie =  "sudo certbot --apache -d "+dominioInstancia
        (stdin, stdout, stderr) = c.exec_command(comandoSie, get_pty = True)


        print ("Esperando...")
        
        print ("1")
        print ("2")
        print ("3")
        stdin.write('2')
        stdin.write('\n')
        stdin.flush()
        cmd_output = stdout.read()
        print("log printing::::\n", comandoSie, cmd_output)

 
        
        c.close()

        return True;

class creacionInstancia(Resource):
    def get(self):
        ec2 = boto3.resource('ec2')
        instances = ec2.create_instances(
            ImageId = 'ami-0eb84a82894801519',
            MaxCount = 1,
            MinCount = 1,
            InstanceType = 't2.micro',
            SecurityGroups = [
                'launch-wizard-2'
            ],
            KeyName = 'iposbyivan',
            Placement = {
                'AvailabilityZone' : 'us-east-1a'
            }
        )
        for instance in instances:
            return instance.id
        



def id_generator(size=8, chars=string.ascii_lowercase):
    return ''.join(random.choice(chars) for _ in range(size))


class creacionDeRuta(Resource):
    def get(self,dominioInstancia):
        inicioRuta = id_generator()
        route53 = boto3.client('route53')
        response = route53.change_resource_record_sets(
            HostedZoneId='Z31FBEPFCFECUP',
            ChangeBatch={
                'Comment': 'test',
                'Changes': [
                {
                    'Action': 'CREATE',
                    'ResourceRecordSet': {
                        'Name': inicioRuta +'.ivanapps.com',
                        'Type': 'CNAME',
                        'TTL':300,
                        'ResourceRecords': [
                            {
                                'Value': dominioInstancia
                            },
                        ],
                        
                      
                        
                    }
                }]
            }
        )
        return inicioRuta
        


class listado(Resource):
    def get(self):
        route53 = boto3.client('route53')
        response = route53.list_resource_record_sets(
            HostedZoneId='Z31FBEPFCFECUP',
        )

        return response

class statusInstancia(Resource):
    def get(self,idInstancia):
        
        ec2 = boto3.client('ec2')
        response = ec2.describe_instance_status(
            InstanceIds=[
                idInstancia,
            ],
        )
        #for instance in response['Reservations']:
        #    for instances in instance["Instances"]:
        #        ec2 = boto3.resource('ec2')
        #        specificinstance = ec2.Instance(instances["InstanceId"])
        return response



#class detalleInstacia(Resource):
 #   def get(self):
  #      ec2 = boto3.resource('ec2')
   #     instance = ec2.Instance('i-0ee4f7c3cad4d1c2b')
    #    print(instance)
            
        #network_interfaces_attribute
    
class licenciaEncript(Resource):
    def get(self):
        #
        key =  Fernet.generate_key() 
            #private static $salt ='S3cr3tK3Y@iPOSbyIVANL1c3ns3S-v11';

        cadena = '@@4@@https://ishop-test1.ivanapps.com/'
        cadena = cadena.encode()
        f = Fernet(key)
        cadena = f.encrypt(cadena)
        return jsonify(print(cadena))





class iniciarInstancia(Resource):
    def get(self,idInstancia):
        ec2 = boto3.resource('ec2')
        instance = ec2.Instance(idInstancia)
        response = instance.start(
            
        )
        return response

class detenerInstancia(Resource):
    def get(self,idInstancia):
        ec2 = boto3.resource('ec2')
        instance = ec2.Instance(idInstancia)
        response = instance.stop()
        return response

class reiniciarInstancia(Resource):
    def get(self,idInstancia):
        ec2 = boto3.resource('ec2')
        instance = ec2.Instance(idInstancia)
        response = instance.reboot()
        return response

class terminarInstancia(Resource):
    def get(self,idInstancia):
        ec2 = boto3.resource('ec2')
        instance = ec2.Instance(idInstancia)
        response = instance.terminate()
        return response


class carnitaInstancia(Resource):
    def get(self, idInstancia):
        ec2 = boto3.client('ec2')
        response = ec2.describe_instances(
            InstanceIds=[
                idInstancia,
            ],
            
        )
        return jsonify(response)




    


api.add_resource(Quotes, '/')
api.add_resource(creacionInstancia,'/crear_instancia')
api.add_resource(iniciarInstancia,'/iniciar_instancia/<idInstancia>')
api.add_resource(detenerInstancia,'/detener_instancia/<idInstancia>')
api.add_resource(reiniciarInstancia,'/reiniciar_instancia/<idInstancia>')
api.add_resource(terminarInstancia,'/terminar_instancia/<idInstancia>')
api.add_resource(carnitaInstancia,'/detalle_instancia/<idInstancia>')
api.add_resource(statusInstancia,'/status_instancia/<idInstancia>')
api.add_resource(licenciaEncript,'/generar_licencia')
api.add_resource(creacionDeRuta,'/crear_dominio/<dominioInstancia>')

api.add_resource(connectSsh,'/conexion_server/<dominioInstancia>')

api.add_resource(listado,'/listado')

api.add_resource(descargarArchivo,'/descargar/<dominioInstancia>')
api.add_resource(escribirArchivo,'/escribir/<dominioInstancia>')
api.add_resource(actualizarArchivo,'/actualizar/<dominioInstancia>')
api.add_resource(comandActualizar,'/actualizar-config/<dominioInstancia>')


api.add_resource(descargarIndex,'/descargar-index/<dominioInstancia>')
api.add_resource(escribirIndex,'/escribir-index/<dominioInstancia>')
api.add_resource(actualizarIndex,'/actualizar-index/<dominioInstancia>')
api.add_resource(comandActualizarIndex,'/actualizar-file-index/<dominioInstancia>')


api.add_resource(descargarConf,'/descargar-conf/<dominioInstancia>')
api.add_resource(escribirConf,'/escribir-conf/<dominioInstancia>')
api.add_resource(crearCarpeta,'/crear-carpeta/<dominioInstancia>')
api.add_resource(actualizarConf,'/actualizar-conf/<dominioInstancia>')
#api.add_resource(testConnectionDB,'/test-db')
api.add_resource(ultimosCommands,'/ultimos-comandos/<dominioInstancia>')
api.add_resource(cerrandoInstanciaCreada,'/cerrando/<dominioInstancia>')
api.add_resource(resetCerbot,'/cerbot//<dominioInstancia>')

if __name__ == '__main__':
    app.run(debug=True)